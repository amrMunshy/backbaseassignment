package com.backbase.backbaseassignment.Fragment;

import android.support.test.rule.ActivityTestRule;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.backbase.backbaseassignment.Activity.MainActivity;
import com.backbase.backbaseassignment.R;

import org.junit.Assert;

public class FragmentTestRule<F extends Fragment> extends ActivityTestRule<MainActivity> {

    private final Class<F> mFragmentClass;
    private F mFragment;

    public FragmentTestRule(final Class<F> fragmentClass) {
        super(MainActivity.class, true, false);
        mFragmentClass = fragmentClass;
    }

    @Override
    protected void afterActivityLaunched() {
        super.afterActivityLaunched();
        try {
            //Instantiate and insert the fragment into the container layout
            FragmentManager manager = getActivity().getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            mFragment = mFragmentClass.newInstance();
            transaction.replace(R.id.lnrLayoutContainerFirstFragment, mFragment);
            transaction.commit();
        } catch (InstantiationException | IllegalAccessException e) {
            Assert.fail(String.format("%s: Could not insert %s into TestActivity: %s",
                    getClass().getSimpleName(),
                    mFragmentClass.getSimpleName(),
                    e.getMessage()));
        }
    }
}
