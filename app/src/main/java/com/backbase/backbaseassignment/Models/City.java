package com.backbase.backbaseassignment.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class City {
    @SerializedName("country")
    @Expose
    public String country;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("_id")
    @Expose
    public Integer id;
    @SerializedName("coord")
    @Expose
    public Coord coord;

    @Override
    public String toString() {
        return name + "," + country;
    }
}
