package com.backbase.backbaseassignment.SearchCity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.backbase.backbaseassignment.Activity.OnCityClickListener;
import com.backbase.backbaseassignment.Models.City;
import com.backbase.backbaseassignment.R;
import com.backbase.backbaseassignment.Utils.EditTextWatcher;
import com.backbase.backbaseassignment.Utils.Utilities;

import java.util.ArrayList;

public class SearchCityFragment extends Fragment implements SearchCityView {
    private OnCityClickListener onCityClickListener;
    private EditText edtSearch;
    private TextView txtCaution;
    private ImageView imgClearSearch;
    private RecyclerView rvCities;
    private SearchCityPresenter searchCityPresenter;
    private Context context;

    private SearchCityAdapter searchCityAdapter;

    /**
     * empty constructor required for Android system to instantiate the fragment
     */
    public SearchCityFragment() {
    }

    /**
     * creating new instance of SearchCityFragment
     *
     * @return SearchCityFragment
     */
    public static SearchCityFragment newInstance() {
        Bundle args = new Bundle();
        SearchCityFragment fragment = new SearchCityFragment();
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * attaching the interface passed from the activity to communicate with it
     *
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onCityClickListener = (OnCityClickListener) context;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_search_city, container, false);
        initializeViews(v);
        return v;
    }

    /**
     * called when the activity has been created successfully and all the views have been initialized
     *
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity();
        searchCityPresenter = new SearchCityPresenter(context, this);
        searchCityPresenter.execute();
    }

    /**
     * initializing the views
     *
     * @param v
     */
    private void initializeViews(View v) {
        rvCities = v.findViewById(R.id.rvCities);
        edtSearch = v.findViewById(R.id.edtSearch);
        txtCaution = v.findViewById(R.id.txtCaution);
        imgClearSearch = v.findViewById(R.id.imgClearSearch);
    }

    /**
     * setting the listeners to the views
     */
    private void setListeners() {
        EditTextWatcher editTextWatcher = new EditTextWatcher(searchCityAdapter, imgClearSearch);
        edtSearch.addTextChangedListener(editTextWatcher);
        imgClearSearch.setOnClickListener(clearClickListener);
    }

    /**
     * show all the cities retrieved from the presenter
     *
     * @param cityArrayList
     */
    @Override
    public void showAllCities(ArrayList<City> cityArrayList) {
        if (cityArrayList != null && cityArrayList.size() != 0) {
            txtCaution.setVisibility(View.GONE);
            rvCities.setVisibility(View.VISIBLE);
            searchCityAdapter = new SearchCityAdapter(context, this, cityArrayList);
            rvCities.setLayoutManager(new LinearLayoutManager(context));
            rvCities.setAdapter(searchCityAdapter);
            setListeners();
        } else {
            noResultsFound();
        }
    }

    /**
     * showing that no results found to match the user search keyword
     */
    @Override
    public void noResultsFound() {
        txtCaution.setVisibility(View.VISIBLE);
        rvCities.setVisibility(View.GONE);
        txtCaution.setText(getString(R.string.no_results));
    }

    /**
     * hide the message and show the newly found results
     */
    @Override
    public void ResultsFound() {
        txtCaution.setVisibility(View.GONE);
        rvCities.setVisibility(View.VISIBLE);
    }

    /**
     * close the keyboard after clicking on a city and passing it to show it on the map
     *
     * @param city
     */
    @Override
    public void onItemClickListener(City city) {
        Utilities.hideKeyboard((Activity) context);
        onCityClickListener.onCityClick(city);
    }

    View.OnClickListener clearClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            edtSearch.setText("");
        }
    };
}
