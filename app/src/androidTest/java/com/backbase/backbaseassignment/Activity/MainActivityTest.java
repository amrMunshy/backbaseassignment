package com.backbase.backbaseassignment.Activity;

import android.support.test.rule.ActivityTestRule;

import com.backbase.backbaseassignment.CityLocation.CityLocationFragment;
import com.backbase.backbaseassignment.R;
import com.backbase.backbaseassignment.SearchCity.SearchCityFragment;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public class MainActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void onCreate() {
        onView(withId(R.id.lnrLayoutContainerFirstFragment)).check(matches(isDisplayed()));
        onView(withId(R.id.lnrLayoutContainerSecondFragment)).check(matches(isDisplayed()));
        SearchCityFragment searchCityFragment = new SearchCityFragment().newInstance();
        CityLocationFragment cityFragment = new CityLocationFragment().newInstance();
        mActivityRule.getActivity().getSupportFragmentManager().beginTransaction().add(R.id.lnrLayoutContainerFirstFragment, searchCityFragment).commit();
        mActivityRule.getActivity().getSupportFragmentManager().beginTransaction().add(R.id.lnrLayoutContainerFirstFragment, cityFragment).commit();
    }
}