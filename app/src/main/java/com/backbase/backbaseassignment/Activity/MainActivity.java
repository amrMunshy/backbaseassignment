package com.backbase.backbaseassignment.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.backbase.backbaseassignment.CityLocation.CityLocationFragment;
import com.backbase.backbaseassignment.Models.City;
import com.backbase.backbaseassignment.R;
import com.backbase.backbaseassignment.SearchCity.SearchCityFragment;
import com.backbase.backbaseassignment.Utils.Constants;
import com.backbase.backbaseassignment.Utils.Utilities;

public class MainActivity extends AppCompatActivity implements OnCityClickListener {
    private SearchCityFragment searchCityFragment;
    private CityLocationFragment cityLocationFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadFirstFragment();
        loadSecondFragment();
    }

    private void loadFirstFragment() {
        searchCityFragment = new SearchCityFragment();
        Utilities.addFragmentToActivity(searchCityFragment, Constants.SEARCH_CITY, getSupportFragmentManager(), null, R.id.lnrLayoutContainerFirstFragment);
    }

    private void loadSecondFragment() {
        cityLocationFragment = CityLocationFragment.newInstance();
        Utilities.addFragmentToActivity(cityLocationFragment, Constants.CITY_LOCATION, getSupportFragmentManager(), null, R.id.lnrLayoutContainerSecondFragment);
    }


    @Override
    public void onCityClick(City city) {
        cityLocationFragment.onCityClicked(city);
    }
}
