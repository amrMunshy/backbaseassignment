package com.backbase.backbaseassignment.SearchCity;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.backbase.backbaseassignment.Models.City;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SearchCityPresenter extends AsyncTask<Void, Void, ArrayList<City>> {
    private Context context;
    private ArrayList<City> cities;
    private String ASSETS_CITIES_FILE = "cities.json";
    private SearchCityView searchCityView;

    /**
     * constructing the presenter class with:
     *
     * @param context
     * @param searchCityView
     */
    public SearchCityPresenter(Context context, SearchCityView searchCityView) {
        this.context = context;
        this.searchCityView = searchCityView;
    }

    /**
     * sorting all the cities
     *
     * @return ArrayList<City>
     */
    public ArrayList<City> sortCities() {
        Collections.sort(cities, new Comparator<City>() {
            public int compare(City o1, City o2) {
                return o1.name.compareTo(o2.name);
            }
        });
        return cities;
    }

    /**
     * read the file as inputstream , estimating the number of bytes that can be read,converting it to byte[]
     * generating the String that have all the file data and converting it to ArrayList usin Gson library
     *
     * @param voids
     * @return all the cities sorted
     */
    @Override
    protected ArrayList<City> doInBackground(Void... voids) {
        try {
            InputStream is = context.getAssets().open(ASSETS_CITIES_FILE);
            int size = 0;
            size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String str_data = new String(buffer);
            Gson gson = new Gson();
            Type listType = new TypeToken<List<City>>() {
            }.getType();
            if (!TextUtils.isEmpty(str_data))
                cities = gson.fromJson(str_data, listType);
            else {
                cities = new ArrayList<>();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sortCities();
    }

    /**
     * retrieving all of the cities sorted and invoking the view interface to show the cities
     *
     * @param cities
     */
    @Override
    protected void onPostExecute(ArrayList<City> cities) {
        searchCityView.showAllCities(cities);
    }
}
