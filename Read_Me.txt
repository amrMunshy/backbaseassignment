-BackBaseAssignment

An application that lists most of big cities in all the countries along with it's coordinates, the user can search among them in real time and showing him the exact location of the desired city in the same screen.


-Prerequisites

An android device with "at least" android 4.1(Jelly Bean) with google play services installed on it

-How to install

1-Download the APK in the following :  https://drive.google.com/open?id=1BoC9i9ozRZYwfxx7HT95hQxSuxOvL-IF
2-Navigate to settings, security, unknown sources, selecting this option will allow you to install apps outside of the Google Play store
3-install the downloaded file


-Tutorial:
Kindly find the following link for a video for a demo of the application: https://drive.google.com/open?id=1CMO0ldkSr5QdCr3W1Y6vSoDXN_n6cZYE