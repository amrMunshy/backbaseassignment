package com.backbase.backbaseassignment.CityLocation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.backbase.backbaseassignment.Models.City;
import com.backbase.backbaseassignment.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class CityLocationFragment extends Fragment implements OnMapReadyCallback {
    private MapView mapView;
    private GoogleMap mMap;

    /**
     * empty constructor required for Android system to instantiate the fragment
     */
    public CityLocationFragment() {
    }

    /**
     * creating new instance of CityLocationFragment
     *
     * @return CityLocationFragment
     */
    public static CityLocationFragment newInstance() {
        Bundle args = new Bundle();
        CityLocationFragment fragment = new CityLocationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_map, container, false);
        initializeViews(v);
        setHasOptionsMenu(true);
        return v;
    }

    /**
     * initializing the views
     *
     * @param v
     */
    private void initializeViews(View v) {
        mapView = v.findViewById(R.id.map);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);
    }

    /**
     * called when the map is ready to be used and locating the marker to amsterdam
     *
     * @param googleMap
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng sydney = new LatLng(52.3680, 4.9036);
        mMap.addMarker(new MarkerOptions().position(sydney));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    /**
     * called when the user chooses a city and locating the marker to it
     *
     * @param city
     */
    public void onCityClicked(City city) {
        mMap.clear();
        LatLng clickedCityLocation = new LatLng(city.coord.lat, city.coord.lon);
        mMap.addMarker(new MarkerOptions().position(clickedCityLocation)).setTitle(city.name);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(clickedCityLocation));

    }
}
