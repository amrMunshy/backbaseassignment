package com.backbase.backbaseassignment.Utils;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class Utilities {
    /**
     * find the fragment by the tag and add the bundle to it's arguments then start the transaction
     *
     * @param fragment
     * @param fragmentTag
     * @param fragmentManager
     * @param bundle
     * @param linearLayout
     */
    public static void addFragmentToActivity(Fragment fragment, String fragmentTag, FragmentManager fragmentManager, Bundle bundle, int linearLayout) {
        Fragment myFragment;
        myFragment = fragmentManager.findFragmentByTag(fragmentTag);
        if (myFragment == null) {
            if (bundle != null) {
                fragment.setArguments(bundle);
            }
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(linearLayout, fragment, fragmentTag).commit();
        }
    }

    /**
     * hiding the keyboard from the screen
     * @param activity
     */
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
