package com.backbase.backbaseassignment.Activity;

import com.backbase.backbaseassignment.Models.City;

public interface OnCityClickListener {
    void onCityClick(City city);
}
