package com.backbase.backbaseassignment.SearchCity;

import com.backbase.backbaseassignment.Models.City;

import java.util.ArrayList;

public interface SearchCityView {
    void showAllCities(ArrayList<City> cityArrayList);
    void noResultsFound();
    void ResultsFound();
    void onItemClickListener(City city);

}
