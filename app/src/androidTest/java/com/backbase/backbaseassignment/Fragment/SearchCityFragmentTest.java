package com.backbase.backbaseassignment.Fragment;

import com.backbase.backbaseassignment.R;
import com.backbase.backbaseassignment.SearchCity.SearchCityFragment;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public class SearchCityFragmentTest {
    @Rule
    public FragmentTestRule<SearchCityFragment> searchCityFragmentFragmentTestRule = new FragmentTestRule<>(SearchCityFragment.class);
    @Test
    public void whenFragmentStart() {
        // Launch the activity to make the fragment visible
        searchCityFragmentFragmentTestRule.launchActivity(null);
        // Then use Espresso to test the Fragment
        onView(withId(R.id.edtSearch)).check(matches(isDisplayed()));
        onView(withId(R.id.rvCities)).check(matches(isDisplayed()));
        onView(withId(R.id.edtSearch))
                .perform(typeText("cairo"), closeSoftKeyboard());
        onView(withId(R.id.edtSearch))
                .perform(clearText(), closeSoftKeyboard());
        onView(withId(R.id.edtSearch))
                .perform(typeText("toronto"), closeSoftKeyboard());
        onView(withId(R.id.edtSearch))
                .perform(clearText(), closeSoftKeyboard());
        onView(withId(R.id.edtSearch))
                .perform(typeText("      "), closeSoftKeyboard());

    }


}
