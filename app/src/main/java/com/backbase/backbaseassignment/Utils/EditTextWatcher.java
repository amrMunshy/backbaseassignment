package com.backbase.backbaseassignment.Utils;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;

import com.backbase.backbaseassignment.SearchCity.SearchCityAdapter;

public class EditTextWatcher implements TextWatcher {
    private SearchCityAdapter searchCityAdapter;
    private ImageView imgClearSearch;

    public EditTextWatcher(SearchCityAdapter searchCityAdapter, ImageView imgClearSearch) {
        this.searchCityAdapter = searchCityAdapter;
        this.imgClearSearch = imgClearSearch;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    /**
     * start filtering after each character the user types
     *
     * @param editable
     */
    @Override
    public void afterTextChanged(Editable editable) {
        String keyWord = String.valueOf(editable);
        if (keyWord != null) {
            if (!TextUtils.isEmpty(keyWord)) {
                imgClearSearch.setVisibility(View.VISIBLE);
            } else {
                imgClearSearch.setVisibility(View.GONE);
            }
            searchCityAdapter.getFilter().filter(editable.toString());
        }
    }
}
