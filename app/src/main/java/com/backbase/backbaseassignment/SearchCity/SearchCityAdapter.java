package com.backbase.backbaseassignment.SearchCity;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.backbase.backbaseassignment.Models.City;
import com.backbase.backbaseassignment.R;

import java.util.ArrayList;

public class SearchCityAdapter extends RecyclerView.Adapter<SearchCityAdapter.ViewHolder> implements Filterable {
    private Context context;
    private ArrayList<City> cityArrayList;
    private ArrayList<City> filteredCityArrayList;
    private SearchCityView searchCityView;

    public SearchCityAdapter(Context context, SearchCityView searchCityView, ArrayList<City> cityArrayList) {
        this.context = context;
        this.cityArrayList = cityArrayList;
        this.filteredCityArrayList = cityArrayList;
        this.searchCityView = searchCityView;
    }

    @Override
    public SearchCityAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_city, parent, false);
        final SearchCityAdapter.ViewHolder mViewHolder = new SearchCityAdapter.ViewHolder(view);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(SearchCityAdapter.ViewHolder holder, int position) {
        City city = filteredCityArrayList.get(position);
        holder.txtCity.setText(city.toString());
        holder.lnrCity.setTag(position);
        holder.lnrCity.setOnClickListener(onCityClickListener);
    }

    @Override
    public int getItemCount() {
        return filteredCityArrayList.size();
    }

    /**
     * filtering the cities after each character the user types
     * showing him the results found and in case there were none,show him disclaimer
     * @return
     */
    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                if (results != null && results.count > 0) {
                    filteredCityArrayList = (ArrayList<City>) results.values;
                    notifyDataSetChanged();
                    searchCityView.ResultsFound();
                } else {
                    searchCityView.noResultsFound();
                }
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                ArrayList<City> filterCityArrayList = new ArrayList<>();
                if (constraint != null) {
                    constraint = constraint.toString().trim().toLowerCase();
                    if (!constraint.toString().trim().isEmpty()) {
                        for (int i = 0; i < cityArrayList.size(); i++) {
                            if (cityArrayList.get(i).name.trim().toLowerCase().startsWith(constraint.toString())) {
                                filterCityArrayList.add(cityArrayList.get(i));
                            }
                        }
                    } else {
                        filterCityArrayList = cityArrayList;
                    }
                    results.count = filterCityArrayList.size();
                    results.values = filterCityArrayList;
                }
                return results;

            }
        };
        return filter;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtCity;
        private LinearLayout lnrCity;

        public ViewHolder(View itemView) {
            super(itemView);
            txtCity = itemView.findViewById(R.id.txtCity);
            lnrCity = itemView.findViewById(R.id.lnrCity);
        }
    }

    View.OnClickListener onCityClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getTag() != null) {
                int pos = (int) view.getTag();
                searchCityView.onItemClickListener(filteredCityArrayList.get(pos));
            }
        }
    };

}
